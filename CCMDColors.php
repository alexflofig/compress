<?php

class CCMDColors {

    public static $textColors = array(
        'black' => '0;30',
        'blue'  => '0;34',
        'green' => '0;32',
        'red'   => '0;31',
        'lred'  => '1;31',
        'yellow'=> '1;33',
        'white' => '1;37'
    );


    public static function CMDColor($string,$color){
        $color = strtolower($color);
        if(self::isTextColorAvailable($color)){
            $coloredString = "\033[" . self::$textColors[$color] . 'm' . $string . "\033[0m";
        } else {
            $coloredString = $string;
        }

        return $coloredString;
    }

    /**
     * Returns the list of available text colors to use
     * @return array
     */
    public static function getTextColors(){
        return array_keys(self::$textColors);
    }

    /**
     * Returns TRUE if Color is available as text color
     * @param $color
     * @return bool
     */
    public static function isTextColorAvailable($color){
        return !empty(self::$textColors[$color]);
    }


}