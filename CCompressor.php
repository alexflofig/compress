<?php
require 'CDictionary.php';
class CCompressor {

    private $_wordBoundaries;

    public function __construct(){
        $this->_wordBoundaries = array( ' ', "\n", "\t" );
    }

    /**
     * Compression
     * Compression Rules:
     * - Espace, line break and tabs are NOT compressed.
     * - Every word is searched in dictionary, if present, replaced with dictionary key, if not, added into
     * -   custom dictionary and compressed. If dictionary already reach 127 keys, Escape Word
     * @param $filename
     * @param $compressTo
     * @return mixed
     * @throws Exception If Unable to open file for reading
     */
    public function compress($filename,$compressTo){

        $fpFile = @fopen($filename,'r');
        if(!$fpFile){
            throw new Exception("Unable to open $filename for reading. ");
        }

        $fpCompressedFile = @fopen($compressTo,'x');
        if(!$fpCompressedFile){
            fclose($fpFile);
            throw new Exception("Unable to write the compressed file $compressTo for writing. ");
        }

        $Dictionary = new CDictionary();
        $compressedDoc = '';
        while( $word = $this->_readWord($fpFile) ){
            $compressedDoc .= $Dictionary->compressWord($word);
        }

        fwrite($fpCompressedFile, $Dictionary->getDictionary());
        fwrite($fpCompressedFile, "\n");
        fwrite($fpCompressedFile, $compressedDoc);

        fclose($fpFile);
        fclose($fpCompressedFile);
        return $compressTo;
    }

    /**
     * Reads the file for the next word using the boundaries as word limit
     * and reading all spaces or word boundaries after the word
     * @param $fp
     * @return string
     */
    private function _readWord($fp){
        $wordBuffer = "";
        $spaceBoundaryFound = false;
        while ( false !== ($c = fgetc($fp)) ){
            if(in_array($c,$this->_wordBoundaries) ){
                $spaceBoundaryFound = true;
                $wordBuffer .= $c;
            } else {
                if($spaceBoundaryFound){
                    fseek($fp,-1,SEEK_CUR);
                    return $wordBuffer;
                } else {
                    $wordBuffer .= $c;
                }
            }
        }
        return $wordBuffer;
    }

    /**
     * Reads the compressed file and uncompress it into target filename
     * @param $compressedFilename
     * @param $targetFilename
     * @return bool
     * @throws Exception
     */
    public function unCompress($compressedFilename, $targetFilename){
        $fpCompressedFile = @fopen($compressedFilename,'r');
        if(!$fpCompressedFile){
            throw new Exception("Compressed file is not readable.");
        }

        $fpTargetFile = @fopen($targetFilename,'x');
        if(!$fpTargetFile){
            fclose($fpCompressedFile);
            throw new Exception("Unable to write in the target file.");
        }

        WriteInfoMessage("Uncompressing file $compressedFilename into $targetFilename");

        // The first line of the file, should contain the Dictionary in json format
        $Dictionary = new CDictionary();
        $Dictionary->setDictionary(fgets($fpCompressedFile));

        //Read the compressed tokens from compressed File and save them.
        while(!feof($fpCompressedFile)){
            $word = $this->_readCompressedToken($fpCompressedFile,$Dictionary);
            fwrite($fpTargetFile,$word);
        }


        fclose($fpCompressedFile);
        fclose($fpTargetFile);
        return true;

    }

    private function _readCompressedToken(&$fp, CDictionary &$Dictionary){
        //read 2 bytes by 2 bytes to detect escaping and saved indexes.
        $token = fread($fp,2);
        if($token == false){
            return false;
        }

        $unpacked = unpack('n',$token);
        $index = $unpacked[1];
        if($index > 0){
            return $Dictionary->getWord($index);
        }

        //If $index is 0, then we need to grab characters until another 0 is found
        $word = "";
        while(!feof($fp)){
            $token = fread($fp,1);
            if($token == false){
                break;
            }
            $unpacked = unpack('c',$token);
            if($unpacked[1] != chr(0)){
                $word .= chr($unpacked[1]);
            } else {
                break;
            }
        }
        return $word;

    }


    
}

