<?php

class CDictionary {

    const DICTIONARY_LIMIT = 65534;

    private $_dictionary;
    private $_countDictionary;

    public function __construct(){
        $this->_dictionary      = array();
        $this->_dictionary[]    = chr(0);
        $this->_countDictionary = 1;
    }

    /**
     * Search the word into the dictionary and if found, returns the key,
     * if not found, tries to add the word into the dictionary and returns the new key,
     * if not able to add the key, return escaped word.
     * @param $word
     * @return int|mixed|string
     */
    public function compressWord($word){

        //Search if word is already stored in the dictionary and return key
        if($key = array_search($word,$this->_dictionary)){
            return pack('n',$key);
        }
        //If word is not in the dictionary, try to store it and return new key
        else {
            if($key = $this->_saveWord($word)){
                return pack('n',$key);
            } else {
                //Key was not able to save, escape word and save it plain
                return pack('a*',chr(0).chr(0).$word.chr(0));
            }
        }
    }

    /**
     * Tryes to save the word into the dictionary
     * @param $word
     * @return int|string
     */
    private function _saveWord($word){
        if($this->_countDictionary < self::DICTIONARY_LIMIT) {
            $this->_dictionary[] = $word;
            $this->_countDictionary ++;
            return $this->_countDictionary - 1;
        } else {
            return false;
        }
    }

    /**
     * Returns thw word for the giving index
     * @param $index
     * @return bool|mixed
     */
    public function getWord($index){
        if(!empty($this->_dictionary[$index])){
            return $this->_dictionary[$index];
        }
        return false;
    }

    /**
     * Returns the json of the dictionary
     * @return string
     */
    public function getDictionary(){
        return json_encode($this->_dictionary);
    }

    /**
     * Sets the dictionary from a json string
     * @param $json
     */
    public function setDictionary($json){
        $this->_dictionary = json_decode($json);
        $this->_countDictionary = count($this->_dictionary);
    }
}