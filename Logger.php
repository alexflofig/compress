<?php

function WriteErrorMessage($message){
    echo CCMDColors::CMDColor('Error: ','red') . $message . PHP_EOL;
}
function WriteInfoMessage($message){
    echo CCMDColors::CMDColor('Info: ','blue') . $message . PHP_EOL;
}
function human_filesize($bytes, $decimals = 2) {
    $factor = floor((strlen($bytes) - 1) / 3);
    if ($factor > 0) $sz = 'KMGT';
    return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[$factor - 1] . 'B';
}