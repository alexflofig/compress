<?php

/**
 * compress.php Compress a file using a custom compressing algorithm defined by model CompressModel.php
 * Receives filenames as arguments from command line like: php compress.php file1.txt file2.txt file3.txt
 * And writes the compressed filenames: file1.txt.cmp file2.txt.cmp file3.txt.cmp
 *
 * Author: Alejandro Flores Figueroa alexflofig@gmail.com
 */


// Check if filenames were passed as arguments
require_once 'CCMDColors.php';
require_once 'Logger.php';
if(count($argv) <= 1){
    WriteErrorMessage("Expected one or more arguments as filenames to compress. ");
    exit;
}

//Check if arguments are actually valid filenames
$filesToCompress = array();
foreach($argv as $filename){
    if($filename == 'compress.php') continue;
    if(file_exists($filename) && is_file($filename)){
        $filesToCompress[] = $filename;
    }
}

if(count($filesToCompress) == 0){
    WriteErrorMessage("The file names provided are not valid. ");
    exit();
}

require_once 'CCompressor.php';

//Compress file by file
foreach($filesToCompress as $filename){
    $compressedFilename = $filename . '.cmp';
    $fileSize = human_filesize(filesize($filename));
    WriteInfoMessage("Compressing file: $filename($fileSize) into $compressedFilename ...");
    $compressor = new CCompressor();
    try{
        $compressor->compress($filename,$compressedFilename);

        if(file_exists($compressedFilename) && is_file($compressedFilename)){
            $fileSize = human_filesize(filesize($compressedFilename));
            $ratio = filesize($compressedFilename) / filesize($filename);
            WriteInfoMessage("Compressed into $compressedFilename($fileSize) ratio:$ratio .");
        }
    } catch (Exception $e){
        WriteErrorMessage($e->getMessage());
    }
}

