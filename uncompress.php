<?php

/**
 * uncompress.php Reverts the compression done by compress.php
 * The script can uncompress 1 file at a time
 * Example: php uncompress log1.txt.cmp log1.txt
 * The script will NOT overwrite the target file if existent
 *
 * Author: Alejandro Flores Figueroa alexflofig@gmail.com
 */

// Check if filenames were passed as arguments
require_once 'CCMDColors.php';
require_once 'Logger.php';
if(count($argv) != 3){
    WriteErrorMessage("Expected two arguments: file to uncompress and target file name");
    exit;
}

//Check if arguments are actually valid filenames
$compressedFilename = $argv[1];
$targetFilename     = $argv[2];

if(!file_exists($compressedFilename) || !is_file($compressedFilename)){
    WriteErrorMessage("Compressed file is not readable.");
    exit();
}

require_once 'CCompressor.php';

$compressor = new CCompressor();
try {
    $compressor->unCompress($compressedFilename, $targetFilename);
} catch(Exception $e){
    WriteErrorMessage($e->getMessage());
}

